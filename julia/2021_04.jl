include("AdventOfCode.jl")

const boardSize = 5
const winner = ones(Bool, boardSize)

mutable struct Board
	numbers::Vector{Int64}
	called::Matrix{Bool}
	hasBingo::Bool
end

function newBoard(numbers)
	return Board(numbers, zeros(Bool, boardSize, boardSize), false)
end

function checkForBingo(b::Board)
	for i = 1:boardSize
		if b.called[i, 1:boardSize] == winner || b.called[1:boardSize, i] == winner
			return true
		end
	end

	return false
end

function mark!(b::Board, number::Int64)::Bool
	if b.hasBingo
		return true
	end

	idx = findfirst(b.numbers .== number)
	if idx !== nothing
		b.called[idx] = true
	end

	return b.hasBingo = checkForBingo(b)
end

function unmarked_numbers(b::Board)
	unmarked = []

	for i in 1:boardSize * boardSize
		if !b.called[i]
			push!(unmarked, b.numbers[i])
		end
	end

	return unmarked
end

struct Player
	boards::Vector{Board}
end

function readData()
	content = String(read("./data/2021_04.txt"))
	elements = split(content, "\n\n")
	numbers = parse.(Int, split(elements[1], ","))
	boards = replace.(elements[2:length(elements)], Regex("\\s+") => " ")
	boardStrings = split.(strip.(boards), " ")
	boardNumbers = map(x -> parse.(Int, x), boardStrings)

	return (numbers, newBoard.(boardNumbers))
end

function step01()
	numbers, boards = readData()

	for calledNumber in numbers
		for board in boards
			if mark!(board, calledNumber)
				unmarked = unmarked_numbers(board)
				score = sum(unmarked) * calledNumber
				return score
			end
		end
	end

	return 0
end

function step02()
	numbers, boards = readData()
	winningBoards = []

	for calledNumber in numbers
		for board in boards
			if !board.hasBingo && mark!(board, calledNumber)
				push!(winningBoards, board => calledNumber)
			end
		end
	end

	lastWinning = winningBoards[length(winningBoards)]
	unmarked = unmarked_numbers(lastWinning[1])
	winningNumber = lastWinning[2]

	return sum(unmarked) * winningNumber
end

AdventOfCode.verify("step01", 65325, step01)
AdventOfCode.verify("step02", 4624, step02)
