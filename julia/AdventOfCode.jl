module AdventOfCode
    using Printf

    function verify(name, expected, f::Function)
        actual = f()

        @printf("%s = %s\n", name, actual)
        
        if actual != expected
            @printf("    FAILED %s expected\n", expected)
        end
    end
end
