include("AdventOfCode.jl")

aryToInts = v -> parse.(Int, v)
numbers   = readlines("./data/2021_01.txt") |> aryToInts
increases = nums -> filter(i -> nums[i] > nums[i-1], 2:length(nums)) |> length
windows   = nums -> map(i -> sum(nums[i-2:i]), 3:length(nums))
step01()  = numbers |> increases
step02()  = numbers |> windows |> increases

AdventOfCode.verify("step01", 1288, step01)
AdventOfCode.verify("step02", 1311, step02)
