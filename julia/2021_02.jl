include("AdventOfCode.jl")

@enum Direction forward up down

struct Command
    direction::Direction
    amount::Int8
end

function commandFromLine(s)
    (dirstr, amountstr) = split(s, " ")

    dire = if dirstr == "forward"
        forward
    elseif dirstr == "up"
        up
    elseif dirstr == "down"
        down
    else
        throw(DomainError("unknown directory encountered"))
    end

    Command(dire, parse(Int, amountstr))
end

function readCommandsFromFile()
    commandFromLine.(readlines("./data/2021_02.txt"))
end

function step01()
    commands = readCommandsFromFile()
    horizontal = 0
    depth = 0

    for command in commands
        if command.direction == forward
            horizontal += command.amount
        elseif command.direction == up
            depth -= command.amount
        else
            depth += command.amount
        end
    end

    horizontal * depth
end

function step02()
    commands = readCommandsFromFile()
    aim = 0
    horizontal = 0
    depth = 0

    for command in commands
        if command.direction == forward
            horizontal += command.amount
            depth += aim * command.amount
        elseif command.direction == up
            aim -= command.amount
        else
            aim += command.amount
        end
    end

    horizontal * depth
end

AdventOfCode.verify("step01", 1947824, step01)
AdventOfCode.verify("step02", 1813062561, step02)
