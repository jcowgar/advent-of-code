include("AdventOfCode.jl")

"Convert a string representing a binary number into individual integers

Eg. \"010110\" -> [0 1 0 1 1 0]
"
binstr_to_ints(s) = [parse(Int, c) for c in s]

"Read the data converting to a Vector{Vector{Int64}}"
read_data() = binstr_to_ints.(readlines("./data/2021_03.txt"))

function most_common(ints, bit)
	zeros = count(x -> x[bit] == 0, ints)
	ones = length(ints) - zeros

	return ones >= zeros ? 1 : 0
end

function step01()
	data = read_data()
	width = length(data[1])
	gamma_nums = map(bit -> most_common(data, bit), 1:width)
	epsilon_nums = map(bit -> 1 - most_common(data, bit), 1:width)

	gamma = join(map(string, gamma_nums))
	epsilon = join(map(string, epsilon_nums))

	gamma_i = parse(Int, gamma, base = 2)
	epsilon_i = parse(Int, epsilon, base = 2)

	return gamma_i * epsilon_i
end

function step02()
	co2_scrubber_rating_nums = read_data()
	oxygen_generator_nums = copy(co2_scrubber_rating_nums)

	bit_filter = 1

	while length(oxygen_generator_nums) > 1
		bit_match = most_common(oxygen_generator_nums, bit_filter)
		filter!(v -> v[bit_filter] == bit_match, oxygen_generator_nums)

		bit_filter += 1
	end

	bit_filter = 1

	while length(co2_scrubber_rating_nums) > 1
		bit_match = 1 - most_common(co2_scrubber_rating_nums, bit_filter)
		filter!(v -> v[bit_filter] == bit_match, co2_scrubber_rating_nums)

		bit_filter += 1
	end

	oxygen_s = join(map(string, oxygen_generator_nums[1]))
	co2_scrubber_s = join(map(string, co2_scrubber_rating_nums[1]))

	oxygen_i = parse(Int, oxygen_s, base = 2)
	co2_scrubber_i = parse(Int, co2_scrubber_s, base = 2)

	return oxygen_i * co2_scrubber_i
end

function main()
	AdventOfCode.verify("step01", 1131506, step01)
	AdventOfCode.verify("step02", 7863147, step02)
end

main()
