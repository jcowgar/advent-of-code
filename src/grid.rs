use std::fmt::Display;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct Position {
    x: usize,
    y: usize,
}

impl Position {
    pub fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }
}

#[derive(Debug)]
pub struct Grid<T> {
    pub points: Vec<Vec<T>>,
    size_x: usize,
    size_y: usize,
}

impl<T> Grid<T> {
    pub fn new(points: Vec<Vec<T>>) -> Self {
        let size_y = points.len();
        let size_x = if size_y == 0 { 0 } else { points[0].len() };

        Self {
            size_x,
            size_y,
            points,
        }
    }

    pub fn size_x(&self) -> usize {
        self.size_x
    }

    pub fn size_y(&self) -> usize {
        self.size_y
    }

    pub fn recompute_size(&mut self) {
        self.size_x = self.points[0].len();
        self.size_y = self.points.len();
    }

    pub fn point(&self, position: &Position) -> &T {
        &self.points[position.y][position.x]
    }

    pub fn point_mut(&mut self, position: &Position) -> &mut T {
        &mut self.points[position.y][position.x]
    }

    pub fn left_of(&self, position: &Position) -> Option<Position> {
        if position.x > 0 {
            Some(Position {
                x: position.x - 1,
                y: position.y,
            })
        } else {
            None
        }
    }

    pub fn right_of(&self, position: &Position) -> Option<Position> {
        if position.x + 1 < self.size_x {
            Some(Position {
                x: position.x + 1,
                y: position.y,
            })
        } else {
            None
        }
    }

    pub fn up_left_from(&self, position: &Position) -> Option<Position> {
        if position.y > 0 && position.x > 0 {
            Some(Position {
                x: position.x - 1,
                y: position.y - 1,
            })
        } else {
            None
        }
    }

    pub fn up_from(&self, position: &Position) -> Option<Position> {
        if position.y > 0 {
            Some(Position {
                x: position.x,
                y: position.y - 1,
            })
        } else {
            None
        }
    }

    pub fn up_right_from(&self, position: &Position) -> Option<Position> {
        if position.y > 0 && position.x + 1 < self.size_x {
            Some(Position {
                x: position.x + 1,
                y: position.y - 1,
            })
        } else {
            None
        }
    }

    pub fn down_left_from(&self, position: &Position) -> Option<Position> {
        if position.y + 1 < self.size_y && position.x > 0 {
            Some(Position {
                x: position.x - 1,
                y: position.y + 1,
            })
        } else {
            None
        }
    }

    pub fn down_from(&self, position: &Position) -> Option<Position> {
        if position.y + 1 < self.size_y {
            Some(Position {
                x: position.x,
                y: position.y + 1,
            })
        } else {
            None
        }
    }

    pub fn down_right_from(&self, position: &Position) -> Option<Position> {
        if position.y + 1 < self.size_y && position.x + 1 < self.size_x {
            Some(Position {
                x: position.x + 1,
                y: position.y + 1,
            })
        } else {
            None
        }
    }
}

impl<T: Display> Grid<T> {
    pub fn print(&self) {
        for y in &self.points {
            for x in y {
                print!("{: ^3} ", x);
            }
            println!();
        }

        println!();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let points = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        let g = Grid::new(points);

        assert_eq!(g.size_x(), 3, "size_x is incorrect");
        assert_eq!(g.size_y(), 3, "size_y is incorrect");
    }

    #[test]
    fn test_directions_from_center() {
        let points = vec![vec![1, 2, 3], vec![4, 5, 6], vec![7, 8, 9]];
        let g = Grid::new(points);

        let cp = Position::new(1, 1);
        assert_eq!(g.point(&cp), &5, "center square should be 5");

        let p = g.up_left_from(&cp).unwrap();
        assert_eq!(g.point(&p), &1, "center up/left");

        let p = g.up_from(&cp).unwrap();
        assert_eq!(g.point(&p), &2, "center up");

        let p = g.up_right_from(&cp).unwrap();
        assert_eq!(g.point(&p), &3, "center up/right");

        let p = g.left_of(&cp).unwrap();
        assert_eq!(g.point(&p), &4, "left");

        let p = g.right_of(&cp).unwrap();
        assert_eq!(g.point(&p), &6, "right");

        let p = g.down_left_from(&cp).unwrap();
        assert_eq!(g.point(&p), &7, "center down/left");

        let p = g.down_from(&cp).unwrap();
        assert_eq!(g.point(&p), &8, "center down");

        let p = g.down_right_from(&cp).unwrap();
        assert_eq!(g.point(&p), &9, "center down/right");
    }
}
