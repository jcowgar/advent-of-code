pub mod grid;

use std::time::Instant;

pub fn bench<F>(func: F)
where
    F: Fn(),
{
    let now = Instant::now();

    func();

    let elapsed = now.elapsed();

    if elapsed.as_micros() < 1000 {
        println!(" in {}µs", elapsed.as_micros());
    } else if elapsed.as_millis() < 1000 {
        println!(" in {:03}ms", elapsed.as_micros() as f64 / 1000.0);
    } else {
        println!(" in {:03}s", elapsed.as_millis() as f64 / 1000.0);
    }
}
