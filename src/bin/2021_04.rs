use aoc::bench;

use regex::Regex;

struct DataFile {
    called_numbers: Vec<u64>,
    boards: Vec<BingoBoard>,
}

#[derive(Debug)]
struct BingoBoard {
    numbers: Vec<u64>,
    marked_positions: Vec<usize>,
}

impl BingoBoard {
    pub fn is_winner(&self) -> bool {
        let winners = vec![
            // Rows
            vec![0, 1, 2, 3, 4],
            vec![5, 6, 7, 8, 9],
            vec![10, 11, 12, 13, 14],
            vec![15, 16, 17, 18, 19],
            vec![20, 21, 22, 23, 24],
            // Columns
            vec![0, 5, 10, 15, 20],
            vec![1, 6, 11, 16, 21],
            vec![2, 7, 12, 17, 22],
            vec![3, 8, 13, 18, 23],
            vec![4, 9, 14, 19, 24],
        ];

        'check_winner: for winner in winners {
            for number in winner {
                if !self.marked_positions.contains(&number) {
                    continue 'check_winner;
                }
            }

            return true;
        }

        false
    }

    pub fn mark(&mut self, number: u64) -> bool {
        if let Some(index) = self.numbers.iter().position(|x| x == &number) {
            self.marked_positions.push(index);

            return self.is_winner();
        }

        false
    }

    pub fn unmarked_numbers(&self) -> Vec<u64> {
        let mut unmarked_numbers = vec![];

        for i in 0usize..self.numbers.len() {
            // do something
            if !self.marked_positions.contains(&i) {
                unmarked_numbers.push(self.numbers[i]);
            }
        }

        unmarked_numbers
    }

    pub fn winning_number(&self) -> u64 {
        let last_marked = self.marked_positions.last().unwrap();
        self.numbers[*last_marked]
    }
}

fn read_data_file() -> DataFile {
    let number_regex = Regex::new(r"(\d+)").unwrap();

    let data: Vec<String> = std::fs::read_to_string("./data/2021_04.txt")
        .unwrap()
        .split("\n\n")
        .map(|s| s.to_string())
        .collect();

    let called_numbers: Vec<u64> = data[0]
        .split(',')
        .map(|n| n.to_string().parse::<u64>().unwrap())
        .collect();

    let boards: Vec<BingoBoard> = data
        .iter()
        .skip(1)
        .map(|raw_board| {
            number_regex
                .captures_iter(raw_board)
                .map(|capture| capture[1].to_string())
                .collect::<Vec<String>>()
        })
        .map(|numbers| numbers.iter().map(|n| n.parse::<u64>().unwrap()).collect())
        .map(|numbers| BingoBoard {
            numbers,
            marked_positions: vec![],
        })
        .collect();

    DataFile {
        called_numbers,
        boards,
    }
}

fn step_01() -> u64 {
    let mut data = read_data_file();

    for call_number in data.called_numbers {
        for board in &mut data.boards {
            if board.mark(call_number) {
                let unmarked = board.unmarked_numbers();
                let score = unmarked.iter().sum::<u64>() * call_number;

                return score;
            }
        }
    }

    0
}

fn step_02() -> u64 {
    let mut data = read_data_file();
    let mut last_winning_board_index: Vec<usize> = vec![];

    for call_number in data.called_numbers {
        for (index, board) in &mut data.boards.iter_mut().enumerate() {
            if !last_winning_board_index.contains(&index) && board.mark(call_number) {
                last_winning_board_index.push(index);
            }
        }
    }

    let idx = last_winning_board_index.last().unwrap();
    let last_winning_board = &data.boards[*idx];

    last_winning_board.unmarked_numbers().iter().sum::<u64>() * last_winning_board.winning_number()
}

fn main() {
    bench(|| {
        let step_01_i = step_01();
        assert_eq!(step_01_i, 65325);
        print!("step_01 = {:?}", step_01_i);
    });

    bench(|| {
        let step_02_i = step_02();
        assert_eq!(step_02_i, 4624);
        print!("step_02 = {:?}", step_02_i);
    });
}
