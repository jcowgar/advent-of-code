use aoc::bench;

fn read_data_file() -> Vec<i64> {
    std::fs::read_to_string("./data/2021_07.txt")
        .unwrap()
        .split(',')
        .map(|v| v.parse::<i64>().unwrap())
        .collect()
}

fn calculate_cost<F>(move_cost_fn: F) -> i64
where
    F: Fn(i64, i64) -> i64,
{
    let current_positions = read_data_file();
    let min_position = current_positions.iter().min().unwrap();
    let max_position = current_positions.iter().max().unwrap();
    let mut costs = vec![];

    for position in *min_position..=*max_position {
        costs.push(
            current_positions
                .iter()
                .map(|p| move_cost_fn(*p, position))
                .sum::<i64>(),
        );
    }

    costs.into_iter().min_by(|a, b| a.cmp(b)).unwrap()
}

fn step_01() -> i64 {
    calculate_cost(|a, b| (a - b).abs())
}

fn step_02() -> i64 {
    let precalc_costs: Vec<i64> = (0..2000).map(|x| (1..=x).sum()).collect();

    calculate_cost(|a, b| precalc_costs[(a - b).abs() as usize])
}

fn main() {
    bench(|| {
        let step_01_answer = step_01();
        print!("step_01 = {:?}", step_01_answer);
        // assert_eq!(37, step_01_answer); // Test
        assert_eq!(342641, step_01_answer); // Actual
    });

    bench(|| {
        let step_02_answer = step_02();
        print!("step_02 = {:?}", step_02_answer);
        // assert_eq!(168, step_02_answer); // Test
        assert_eq!(93006301, step_02_answer); // Actual
    });
}
