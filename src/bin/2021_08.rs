use aoc::bench;

struct Sample {
    signal: Vec<String>,
    output: Vec<String>,
}

fn sorted_string(value: &str) -> String {
    let mut chars: Vec<char> = value.chars().collect();
    chars.sort_unstable();
    chars.iter().collect()
}

fn read_data_file() -> Vec<Sample> {
    std::fs::read_to_string("./data/2021_08.txt")
        .unwrap()
        .split('\n')
        .map(|v| {
            let (a, b) = v.split_once(" | ").unwrap();
            Sample {
                signal: a.split(' ').map(sorted_string).collect(),
                output: b.split(' ').map(sorted_string).collect(),
            }
        })
        .collect()
}

fn step_01() -> i64 {
    read_data_file()
        .iter()
        .map(|sample| {
            sample.output.iter().fold(0, |acc, x| {
                acc + match x.len() {
                    2 | 3 | 4 | 7 => 1,
                    _ => 0,
                }
            })
        })
        .sum()
}

impl Sample {
    pub fn find_signal_pattern<F>(&self, len: usize, find_func: F) -> String
    where
        F: Fn(&&String) -> bool,
    {
        self.signal
            .iter()
            .filter(|f| f.len() == len)
            .find(find_func)
            .unwrap()
            .to_string()
    }
}

#[derive(Default)]
struct Mapping {
    a: char, // Top
    b: char, // Upper Left
    c: char, // Upper Right
    d: char, // Center
    e: char, // Lower Left
    f: char, // Lower Right
    g: char, // Bottom
}

pub fn pattern_from_chars(mut chars: Vec<char>) -> String {
    chars.sort_unstable();
    chars.iter().collect()
}

pub fn pattern_add_chars(base: &str, chars: Vec<char>) -> String {
    let mut new_chars = base.chars().chain(chars).collect::<Vec<char>>();
    new_chars.sort_unstable();
    new_chars.iter().collect()
}

fn diff_all(s1: &str, s2: &str) -> String {
    let s1_chars: Vec<char> = s1.chars().collect();
    let s2_chars: Vec<char> = s2.chars().collect();

    let (big, small) = if s1_chars.len() > s2_chars.len() {
        (s1_chars, s2_chars)
    } else {
        (s2_chars, s1_chars)
    };

    big.into_iter()
        .filter(|item| !small.contains(item))
        .collect()
}

fn diff_single(s1: &str, s2: &str) -> char {
    diff_all(s1, s2).chars().next().unwrap()
}

pub fn contains_all(search: &str, each_of: &str) -> bool {
    for each in each_of.chars() {
        if !search.contains(each) {
            return false;
        }
    }

    true
}

fn step_02() -> i64 {
    read_data_file()
        .iter()
        .map(|reading| {
            let mut mapping = Mapping::default();
            let mut patterns = vec![String::from(""); 10];
            patterns[1] = reading.find_signal_pattern(2, |_| true);
            patterns[4] = reading.find_signal_pattern(4, |_| true);
            patterns[7] = reading.find_signal_pattern(3, |_| true);
            patterns[8] = "abcdefg".to_string();

            // Step 1: Solve for Position a = diff in 1 and 7
            mapping.a = diff_single(&patterns[1], &patterns[7]);

            // Step 2: Solve for Number 9 = 4 + a = abdef -> scan for len 6
            let four_and_a = pattern_add_chars(&patterns[4], vec![mapping.a]);

            patterns[9] = reading.find_signal_pattern(6, |v| contains_all(v, &four_and_a));

            mapping.g = diff_single(&patterns[9], four_and_a.as_str());

            // Step 3: Solve for Number 3 = 1 + a & g = abcd -> scan for len 5
            let one_and_ag = pattern_add_chars(&patterns[1], vec![mapping.a, mapping.g]);

            patterns[3] = reading.find_signal_pattern(5, |v| contains_all(v, &one_and_ag));

            mapping.d = diff_single(&patterns[3], &one_and_ag);

            // Step 4: Solve for Number 0 = 8 - d = abcdeg
            patterns[0] = diff_all(&patterns[8], &String::from(mapping.d));

            // Step 5: Solve for Position b = 1 + d = abf, diff against 4
            let one_and_d = pattern_add_chars(&patterns[1], vec![mapping.d]);

            mapping.b = diff_single(&patterns[4], &one_and_d);

            // Step 6: Solve for Number 5 = a+b+d+g = cdef -> scan for len 5
            let five_except_f =
                pattern_from_chars(vec![mapping.a, mapping.b, mapping.d, mapping.g]);

            patterns[5] = reading.find_signal_pattern(5, |v| contains_all(v, &five_except_f));

            mapping.f = diff_single(&patterns[5], &five_except_f);

            // Step 7: Solve for Number 6 = scan for len 6 where is not 0 or 9
            patterns[6] = reading.find_signal_pattern(6, |v| {
                !v.contains(&patterns[9]) && !v.contains(&patterns[0])
            });

            // Step 8: Solve for Position e = diff of 5 & 6
            mapping.e = diff_single(&patterns[5], &patterns[6]);

            // Step 9: Solve for Position c = diff of a + b + d + e + f + g
            let all_except_c = pattern_from_chars(vec![
                mapping.a, mapping.b, mapping.d, mapping.e, mapping.f, mapping.g,
            ]);

            mapping.c = diff_single(&patterns[8], &all_except_c);

            // Step 10: Solve for Number 2 = a+c+d+e+g
            patterns[2] =
                pattern_from_chars(vec![mapping.a, mapping.c, mapping.d, mapping.e, mapping.g]);

            reading
                .output
                .iter()
                .map(|output| patterns.iter().position(|r| r == output))
                .map(|i| i.unwrap().to_string())
                .collect::<Vec<String>>()
                .join("")
                .parse::<i64>()
                .unwrap()
        })
        .sum()
}

fn main() {
    bench(|| {
        let step_01_answer = step_01();
        print!("step_01 = {:?}", step_01_answer);
        // assert_eq!(26, step_01_answer); // Test
        assert_eq!(239, step_01_answer); // Actual
    });

    bench(|| {
        let step_02_answer = step_02();
        print!("step_02 = {:?}", step_02_answer);
        // assert_eq!(61229, step_02_answer); // Test
        assert_eq!(946346, step_02_answer); // Actual
    });
}
