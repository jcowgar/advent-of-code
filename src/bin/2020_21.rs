// Day 21: Allergen Assessment
// https://adventofcode.com/2020/day/21

use std::collections::{HashMap, HashSet};

use aoc::bench;

fn split_to_hashset(line: &str, by: &str) -> HashSet<String> {
    let values: Vec<String> = line.trim().split(by).map(|v| v.to_owned()).collect();

    HashSet::from_iter(values)
}

#[derive(Clone, Debug)]
struct Food {
    allergens: HashSet<String>,
    ingredients: HashSet<String>,
}

impl TryFrom<String> for Food {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let (ingredients_raw, allergens_raw) = value
            .strip_suffix(')')
            .unwrap()
            .split_once("(contains ")
            .unwrap();

        Ok(Food {
            ingredients: split_to_hashset(ingredients_raw, " "),
            allergens: split_to_hashset(allergens_raw, ", "),
        })
    }
}

#[derive(Clone, Debug)]
struct Allergen {
    name: String,
    ingredients: HashSet<String>,
}

impl Allergen {
    fn print(&self) {
        println!("       name: {}", self.name);
        println!("ingredients: {:?}", self.ingredients);
    }
}

fn read_data_file(filename: &str) -> Vec<Food> {
    std::fs::read_to_string(filename)
        .unwrap()
        .split('\n')
        .flat_map(|v| Food::try_from(v.to_owned()))
        .collect()
}

fn compute_ingredients(foods: &[Food]) -> HashSet<String> {
    let mut ingredients = HashSet::new();

    for food in foods {
        ingredients = ingredients
            .union(&food.ingredients)
            .map(|v| v.to_owned())
            .collect();
    }

    ingredients
}

fn compute_allergens(foods: &[Food]) -> Vec<Allergen> {
    let mut allergens = HashMap::<String, Allergen>::new();

    // Collect all known data from foods
    for food in foods {
        for allergen_name in &food.allergens {
            match allergens.get_mut(allergen_name) {
                Some(allergen) => {
                    allergen.ingredients = allergen
                        .ingredients
                        .intersection(&food.ingredients)
                        .map(|v| v.to_owned())
                        .collect();
                }
                None => {
                    allergens.insert(
                        allergen_name.to_owned(),
                        Allergen {
                            name: allergen_name.to_string(),
                            ingredients: HashSet::from_iter(
                                food.ingredients.iter().map(|v| v.to_owned()),
                            ),
                        },
                    );
                }
            }
        }
    }

    let mut result = vec![];

    for (_, v) in allergens {
        result.push(v);
    }

    result
}

fn perform_step_01(filename: &str) -> usize {
    let foods = read_data_file(filename);
    let all_ingredients = compute_ingredients(&foods);
    let allergens = compute_allergens(&foods);
    let mut safe_ingredients = all_ingredients.clone();

    for a in allergens {
        safe_ingredients = safe_ingredients
            .difference(&a.ingredients)
            .map(|v| v.to_owned())
            .collect();
    }

    let mut safe_occurance_count: usize = 0;
    for f in &foods {
        for i in &f.ingredients {
            if safe_ingredients.contains(i) {
                safe_occurance_count += 1;
            }
        }
    }

    safe_occurance_count
}

fn perform_step_02(filename: &str) -> String {
    let foods = read_data_file(filename);
    let mut allergens = compute_allergens(&foods);
    let mut done = vec![];

    while allergens.len() > 0 {
        let mut remove_ingredients = vec![];
        let mut remove_allergen = vec![];

        // Find completed allergens
        for i in 0..allergens.len() {
            let a = allergens[i].clone();

            if a.ingredients.len() == 1 {
                remove_ingredients.push(a.ingredients.iter().next().unwrap().to_owned());
                remove_allergen.push(i);

                done.push(a);
            }
        }

        // Remove the ingredients of known/completed allergens
        for i in &remove_ingredients {
            for a in allergens.iter_mut() {
                a.ingredients.remove(i);
            }
        }

        // Remove the completed allergens from the "unknown" vector.
        for i in remove_allergen.into_iter().rev() {
            allergens.remove(i);
        }
    }

    done.sort_by_key(|v| v.name.to_owned());
    done.iter()
        .map(|v| v.ingredients.iter().next().unwrap().to_owned())
        .collect::<Vec<String>>()
        .join(",")
}

fn step_01_test() -> usize {
    perform_step_01("data/2020_21_test.txt")
}

fn step_01() -> usize {
    perform_step_01("data/2020_21.txt")
}

fn step_02_test() -> String {
    perform_step_02("data/2020_21_test.txt")
}

fn step_02() -> String {
    perform_step_02("data/2020_21.txt")
}

// Kem

fn step_01_kem() -> usize {
    perform_step_01("data/2020_21_kem.txt")
}

fn step_02_kem() -> String {
    perform_step_02("data/2020_21_kem.txt")
}

fn main() {
    bench(|| {
        let answer = step_01_test();
        print!("step_01_test = {:?}", answer);
        assert_eq!(answer, 5, "step_01 test answer");
    });

    bench(|| {
        let answer = step_01();
        print!("step_01 = {:?}", answer);
        assert_eq!(answer, 2075, "step_01 answer");
    });

    bench(|| {
        let answer = step_02_test();
        print!("step_02_test = {:?}", answer);
        assert_eq!(answer, "mxmxvkd,sqjhc,fvjkl", "step_02 test answer");
    });

    bench(|| {
        let answer = step_02();
        print!("step_02 = {:?}", answer);
        assert_eq!(
            answer, "zfcqk,mdtvbb,ggdbl,frpvd,mgczn,zsfzq,kdqls,kktsjbh",
            "step_02 answer"
        );
    });

    bench(|| {
        let answer = step_01_kem();
        print!("step_01_kem = {:?}", answer);
        assert_eq!(answer, 2779, "step_01_kem answer");
    });

    bench(|| {
        let answer = step_02_kem();
        print!("step_02_kem = {:?}", answer);
        assert_eq!(
            answer, "lkv,lfcppl,jhsrjlj,jrhvk,zkls,qjltjd,xslr,rfpbpn",
            "step_02_kem answer"
        );
    });
}
