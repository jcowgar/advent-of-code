use std::collections::HashMap;

use aoc::bench;

#[derive(Debug)]
struct Command {
    chunks: Vec<char>,
    is_valid: bool,
    score: i64,
}

impl Command {
    pub fn new(chunks: Vec<char>) -> Self {
        Self {
            chunks,
            is_valid: true,
            score: 0,
        }
    }
}

fn read_data_file(filename: &str) -> Vec<Command> {
    std::fs::read_to_string(filename)
        .unwrap()
        .split('\n')
        .map(|value| Command::new(value.chars().collect()))
        .collect::<Vec<Command>>()
}

fn get_failure_score(ch: char) -> i64 {
    match ch {
        ')' => 3,
        ']' => 57,
        '}' => 1197,
        '>' => 25137,
        _ => 0,
    }
}

fn step_01(filename: &str) -> i64 {
    let data = read_data_file(filename);
    let mut chunk_map = HashMap::new();

    chunk_map.insert('(', ')');
    chunk_map.insert('[', ']');
    chunk_map.insert('{', '}');
    chunk_map.insert('<', '>');

    let mut illegal_score = 0;

    for mut nav_command in data {
        let mut chunks = vec![];
        let mut nav_command_score = 0;

        for chunk_char in nav_command.chunks {
            if chunk_map.contains_key(&chunk_char) {
                chunks.push(chunk_char);
            } else {
                match chunks.pop() {
                    Some(ch) => {
                        let matching_chunk_char = chunk_map.get(&ch).unwrap();

                        if chunk_char != *matching_chunk_char {
                            nav_command.is_valid = false;
                            nav_command_score += get_failure_score(chunk_char);
                        }
                    }
                    None => {
                        nav_command.is_valid = false;
                        nav_command_score += get_failure_score(chunk_char);
                    }
                }
            }
        }
        illegal_score += nav_command_score;
    }

    illegal_score
}

fn get_complete_score(ch: char) -> i64 {
    match ch {
        ')' => 1,
        ']' => 2,
        '}' => 3,
        '>' => 4,
        _ => 0,
    }
}

fn step_02(filename: &str) -> i64 {
    let mut data = read_data_file(filename);
    let mut chunk_map = HashMap::new();

    chunk_map.insert('(', ')');
    chunk_map.insert('[', ']');
    chunk_map.insert('{', '}');
    chunk_map.insert('<', '>');

    for mut nav_command in &mut data {
        let mut chunks = vec![];

        for chunk_char in &nav_command.chunks {
            if chunk_map.contains_key(chunk_char) {
                chunks.push(chunk_char);
            } else {
                match chunks.pop() {
                    Some(ch) => {
                        let matching_chunk_char = chunk_map.get(ch).unwrap();

                        if chunk_char != matching_chunk_char {
                            nav_command.is_valid = false;
                        }
                    }
                    None => {
                        nav_command.is_valid = false;
                    }
                }
            }
        }

        if nav_command.is_valid {
            let mut command_chunks = nav_command.chunks.clone();

            while let Some(end_chunk) = chunks.pop() {
                let end_ch = chunk_map.get(end_chunk).unwrap();
                command_chunks.push(*end_ch);

                nav_command.score *= 5;
                nav_command.score += get_complete_score(*end_ch);
            }

            nav_command.chunks = command_chunks;
        }
    }

    let mut valid_commands = data
        .into_iter()
        .filter(|command| command.is_valid)
        .collect::<Vec<Command>>();
    valid_commands.sort_by(|a, b| a.score.cmp(&b.score));

    valid_commands[valid_commands.len() / 2].score
}

fn main() {
    bench(|| {
        let step_01_test_answer = step_01("./data/2021_10_test.txt");
        print!("step_01_test_answer = {}", step_01_test_answer);
        assert_eq!(step_01_test_answer, 26397, "step_01 test answer");
    });

    bench(|| {
        let step_01_real_answer = step_01("./data/2021_10.txt");
        print!("step_01_real_answer = {}", step_01_real_answer);
        assert_eq!(step_01_real_answer, 318081, "step_01_real_answer");
    });

    bench(|| {
        let step_02_test_answer = step_02("./data/2021_10_test.txt");
        print!("step_02_test_answer = {}", step_02_test_answer);
        assert_eq!(step_02_test_answer, 288957, "step_02 test answer");
    });

    bench(|| {
        let step_02_real_answer = step_02("./data/2021_10.txt");
        print!("step_02_real_answer = {}", step_02_real_answer,);
        assert_eq!(step_02_real_answer, 4361305341, "step_02 real answer");
    });
}
