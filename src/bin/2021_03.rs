use aoc::bench;

fn read_data_file() -> Vec<Vec<u8>> {
    std::fs::read_to_string("./data/2021_03.txt")
        .unwrap()
        .split('\n')
        .map(|v| v.chars().map(|c| if c == '0' { 0 } else { 1 }).collect())
        .collect()
}

fn most_common(data: &[Vec<u8>], bit: usize) -> u8 {
    let zero_count = data.iter().filter(|v| v[bit] == 0).count();
    let one_count = data.len() - zero_count;

    if one_count >= zero_count {
        1
    } else {
        0
    }
}

fn least_common(data: &[Vec<u8>], bit: usize) -> u8 {
    let zero_count = data.iter().filter(|v| v[bit] == 0).count();
    let one_count = data.len() - zero_count;

    if zero_count <= one_count {
        0
    } else {
        1
    }
}

fn step_01() -> usize {
    let data = read_data_file();
    let gamma = (0..data[0].len())
        .map(|bit| most_common(&data, bit).to_string())
        .collect::<Vec<String>>()
        .join("");
    let epsilon = (0..data[0].len())
        .map(|bit| least_common(&data, bit).to_string())
        .collect::<Vec<String>>()
        .join("");

    let gamma_i = usize::from_str_radix(gamma.as_str(), 2).unwrap();
    let epsilon_i = usize::from_str_radix(epsilon.as_str(), 2).unwrap();

    gamma_i * epsilon_i
}

fn step_02() -> usize {
    let data = read_data_file();
    let mut bit_filter = 0usize;

    let mut oxygen_generator_numbers = data.clone();

    while oxygen_generator_numbers.len() > 1 {
        let bit_match = most_common(&oxygen_generator_numbers, bit_filter);

        oxygen_generator_numbers = oxygen_generator_numbers
            .into_iter()
            .filter(|v| v[bit_filter] == bit_match)
            .collect();

        bit_filter += 1;
    }

    bit_filter = 0usize;
    let mut co2_scrubber_rating_numbers = data;

    while co2_scrubber_rating_numbers.len() > 1 {
        let bit_match = least_common(&co2_scrubber_rating_numbers, bit_filter);

        co2_scrubber_rating_numbers = co2_scrubber_rating_numbers
            .into_iter()
            .filter(|v| v[bit_filter] == bit_match)
            .collect();

        bit_filter += 1;
    }

    let oxygen_s: String = oxygen_generator_numbers[0]
        .iter()
        .map(|v| v.to_string())
        .collect();
    let co2_scrubber_s: String = co2_scrubber_rating_numbers[0]
        .iter()
        .map(|v| v.to_string())
        .collect();

    let oxygen_i = usize::from_str_radix(&oxygen_s, 2).unwrap();
    let co2_scrubber_i = usize::from_str_radix(&co2_scrubber_s, 2).unwrap();

    oxygen_i * co2_scrubber_i
}

fn main() {
    bench(|| {
        let step_01_i = step_01();
        assert_eq!(step_01_i, 1131506);
        print!("step_01 = {:?}", step_01_i);
    });

    bench(|| {
        let step_02_i = step_02();
        assert_eq!(step_02_i, 7863147);
        print!("step_02 = {:?}", step_02_i);
    });
}
