use std::{collections::HashMap, hash::Hash, str::FromStr};

use aoc::bench;

const REGISTER_W: usize = 0;
const REGISTER_X: usize = 1;
const REGISTER_Y: usize = 2;
const REGISTER_Z: usize = 3;

type AluRegister = [i64; 4];

#[derive(Copy, Clone, Default, Eq)]
struct AluState {
    register: AluRegister,
    position: usize,
}

impl Hash for AluState {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.register[REGISTER_Z].hash(state);
        self.position.hash(state);
    }
}

impl PartialEq for AluState {
    fn eq(&self, other: &Self) -> bool {
        self.register[REGISTER_Z] == other.register[REGISTER_Z] && self.position == other.position
    }
}

#[derive(Copy, Clone)]
enum AluValue {
    Register(usize),
    Value(i64),
}

impl AluValue {
    fn value(&self, reg: &AluRegister) -> i64 {
        match self {
            AluValue::Register(register_number) => reg[*register_number],
            AluValue::Value(value) => *value,
        }
    }
}

impl FromStr for AluValue {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "w" => Ok(AluValue::Register(REGISTER_W)),
            "x" => Ok(AluValue::Register(REGISTER_X)),
            "y" => Ok(AluValue::Register(REGISTER_Y)),
            "z" => Ok(AluValue::Register(REGISTER_Z)),
            _ => Ok(AluValue::Value(s.parse::<i64>().unwrap())),
        }
    }
}

enum AluCommand {
    Inp(usize),
    Add(usize, AluValue),
    Mul(usize, AluValue),
    Div(usize, AluValue),
    Mod(usize, AluValue),
    Eql(usize, AluValue),
}

impl FromStr for AluCommand {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (command, parameters) = s.split_once(' ').unwrap();
        let parameters = parameters
            .split(' ')
            .into_iter()
            .map(|v| AluValue::from_str(v).unwrap())
            .collect::<Vec<AluValue>>();

        let register = match parameters[0] {
            AluValue::Register(value) => value,
            _ => panic!("First parameter should always be a register"),
        };

        match command {
            "inp" => Ok(AluCommand::Inp(register)),
            "add" => Ok(AluCommand::Add(register, parameters[1])),
            "mul" => Ok(AluCommand::Mul(register, parameters[1])),
            "div" => Ok(AluCommand::Div(register, parameters[1])),
            "mod" => Ok(AluCommand::Mod(register, parameters[1])),
            "eql" => Ok(AluCommand::Eql(register, parameters[1])),
            _ => Err("Unknown command".to_string()),
        }
    }
}

impl AluCommand {
    fn execute(&self, state: &mut AluState, input: Option<i64>) {
        match self {
            AluCommand::Inp(reg) => state.register[*reg] = input.unwrap(),
            AluCommand::Add(reg, input) => state.register[*reg] += input.value(&state.register),
            AluCommand::Mul(reg, input) => state.register[*reg] *= input.value(&state.register),
            AluCommand::Div(reg, input) => state.register[*reg] /= input.value(&state.register),
            AluCommand::Mod(reg, input) => state.register[*reg] %= input.value(&state.register),
            AluCommand::Eql(reg, input) => {
                state.register[*reg] = (state.register[*reg] == input.value(&state.register)) as i64
            }
        }

        state.position += 1;
    }

    fn is_input(&self) -> bool {
        matches!(self, AluCommand::Inp(_))
    }
}

fn read_data_file(filename: &str) -> Vec<AluCommand> {
    std::fs::read_to_string(filename)
        .unwrap()
        .lines()
        .map(|line| AluCommand::from_str(line).unwrap())
        .collect()
}

fn run(
    input_range: &[i64; 9],
    program: &[AluCommand],
    state: AluState,
    state_cache: &mut HashMap<AluState, Option<i64>>,
) -> Option<i64> {
    if let Some(result) = state_cache.get(&state) {
        return *result;
    }

    'input_loop: for input in input_range {
        let mut state = state;

        program[state.position].execute(&mut state, Some(*input));

        while let Some(command) = program.get(state.position) {
            if command.is_input() {
                match run(input_range, program, state, state_cache) {
                    Some(result) => {
                        state_cache.insert(state, Some(result * 10 + input));
                        return Some(result * 10 + input);
                    }
                    None => continue 'input_loop,
                }
            } else {
                command.execute(&mut state, None);
            }
        }

        if state.register[REGISTER_Z] == 0 {
            state_cache.insert(state, Some(*input));
            return Some(*input);
        }
    }

    state_cache.insert(state, None);

    None
}

fn solve(filename: &str, range: &[i64; 9]) -> i64 {
    let program = read_data_file(filename);

    let mut state_cache = HashMap::new();
    let result = run(range, &program, AluState::default(), &mut state_cache);

    result
        .unwrap()
        .to_string()
        .chars()
        .rev()
        .collect::<String>()
        .parse::<i64>()
        .unwrap()
}

fn step_01(filename: &str) -> i64 {
    solve(filename, &[9, 8, 7, 6, 5, 4, 3, 2, 1])
}

fn step_02(filename: &str) -> i64 {
    solve(filename, &[1, 2, 3, 4, 5, 6, 7, 8, 9])
}

fn main() {
    bench(|| {
        let answer = step_01("./data/2021_24.txt");
        assert_eq!(answer, 99999795919456, "step_01 test answer");
        print!("step_01 answer = {}", answer);
    });

    bench(|| {
        let answer = step_02("./data/2021_24.txt");
        assert_eq!(answer, 45311191516111, "step_02 test answer");
        print!("step_02 answer = {}", answer);
    });

    bench(|| {
        let answer = step_01("./data/2021_24_kem.txt");
        assert_eq!(answer, 49917929934999, "step_01 test answer");
        print!("step_01_kem answer = {}", answer);
    });

    bench(|| {
        let answer = step_02("./data/2021_24_kem.txt");
        assert_eq!(answer, 11911316711816, "step_02 test answer");
        print!("step_02_kem answer = {}", answer);
    });
}
