use aoc::bench;

use std::str::FromStr;

#[derive(Debug)]
enum Command {
    Forward(i64),
    Up(i64),
    Down(i64),
}

impl FromStr for Command {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (a, b) = s.split_once(' ').unwrap();
        let amount = b.parse::<i64>().unwrap();

        Ok(match a {
            "forward" => Command::Forward(amount),
            "up" => Command::Up(amount),
            "down" => Command::Down(amount),
            _ => panic!("Bad input: {}", a),
        })
    }
}

fn read_data_file() -> Vec<Command> {
    std::fs::read_to_string("./data/2021_02.txt")
        .unwrap()
        .split('\n')
        .map(|line| Command::from_str(line).unwrap())
        .collect()
}

fn step_01() -> i64 {
    let commands = read_data_file();
    let mut horizontal = 0;
    let mut depth = 0;

    for command in commands {
        match command {
            Command::Forward(amount) => horizontal += amount,
            Command::Up(amount) => depth -= amount,
            Command::Down(amount) => depth += amount,
        }
    }

    horizontal * depth
}

fn step_02() -> i64 {
    let commands = read_data_file();
    let mut aim = 0;
    let mut horizontal = 0;
    let mut depth = 0;

    for command in commands {
        match command {
            Command::Forward(amount) => {
                horizontal += amount;
                depth += aim * amount
            }
            Command::Up(amount) => aim -= amount,
            Command::Down(amount) => aim += amount,
        }
    }

    horizontal * depth
}

fn main() {
    bench(|| {
        print!("step_01 = {:?}", step_01());
    });

    bench(|| {
        print!("step_02 = {:?}", step_02());
    });
}
