use aoc::bench;

use std::collections::HashMap;
use std::str::FromStr;

fn calculate_inc(from: i64, to: i64) -> i64 {
    match from.cmp(&to) {
        std::cmp::Ordering::Less => 1,
        std::cmp::Ordering::Equal => 0,
        std::cmp::Ordering::Greater => -1,
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    pub fn is_straight_line_to(&self, other: &Point) -> bool {
        self.x == other.x || self.y == other.y
    }

    pub fn line_to(&self, other: &Point) -> Vec<Point> {
        let mut inbetween_points = vec![];

        let x_inc = calculate_inc(self.x, other.x);
        let y_inc = calculate_inc(self.y, other.y);

        let mut x = self.x;
        let mut y = self.y;

        while x != other.x || y != other.y {
            inbetween_points.push(Point { x, y });

            x += x_inc;
            y += y_inc;
        }

        inbetween_points.push(Point { x, y });

        inbetween_points
    }
}

#[derive(Debug)]
struct LineSegment {
    start_point: Point,
    end_point: Point,
    middle_points: Vec<Point>,
}

impl LineSegment {
    pub fn new(start_point: Point, end_point: Point) -> Self {
        Self {
            middle_points: start_point.line_to(&end_point),
            start_point,
            end_point,
        }
    }

    pub fn is_straight_line(&self) -> bool {
        self.start_point.is_straight_line_to(&self.end_point)
    }
}

fn read_data_file() -> Vec<LineSegment> {
    std::fs::read_to_string("./data/2021_05.txt")
        .unwrap()
        .split('\n')
        .map(|v| v.parse::<LineSegment>().unwrap())
        .collect()
}

fn step_01() -> usize {
    let data = read_data_file();
    // Trim the data up to only consider Horizontal and Vertical lines
    let data: Vec<LineSegment> = data
        .into_iter()
        .filter(|line_segment| line_segment.is_straight_line())
        .collect();

    let mut hydrothermal_map = HashMap::new();

    for segment in data {
        for point in segment.middle_points {
            let danger_rating: u64 = match hydrothermal_map.get(&point) {
                Some(value) => *value + 1,
                None => 1,
            };

            hydrothermal_map.insert(point.clone(), danger_rating);
        }
    }

    let danger_point_count = hydrothermal_map.values().filter(|v| *v > &1).count();

    danger_point_count
}

fn step_02() -> usize {
    let data = read_data_file();

    let mut hydrothermal_map = HashMap::new();

    for segment in data {
        for point in segment.middle_points {
            let danger_rating: u64 = match hydrothermal_map.get(&point) {
                Some(value) => *value + 1,
                None => 1,
            };

            hydrothermal_map.insert(point.clone(), danger_rating);
        }
    }

    let danger_point_count = hydrothermal_map.values().filter(|v| *v > &1).count();

    danger_point_count
}

fn main() {
    bench(|| {
        let step_01_answer = step_01();
        //assert_eq!(step_01_answer, 5);
        assert_eq!(step_01_answer, 8622);
        print!("step_01 = {:?}", step_01_answer);
    });

    bench(|| {
        let step_02_answer = step_02();
        // assert_eq!(step_02_answer, 12);
        assert_eq!(step_02_answer, 22037);
        print!("step_02 = {:?}", step_02_answer);
    });
}

impl FromStr for Point {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) = s.split_once(',').unwrap();

        Ok(Point {
            x: x.parse().unwrap(),
            y: y.parse().unwrap(),
        })
    }
}

impl FromStr for LineSegment {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (sp, ep) = s.split_once(" -> ").unwrap();

        Ok(LineSegment::new(sp.parse().unwrap(), ep.parse().unwrap()))
    }
}
