use aoc::bench;
use aoc::grid::{Grid, Position};
use colored::*;
use std::fmt::Display;

#[derive(Debug)]
struct Point {
    risk_level: u32,
    entry_score: Option<u32>,
}

impl Point {
    pub fn new(risk_level: u32) -> Self {
        Point {
            risk_level,
            entry_score: None,
        }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let entry_score = match self.entry_score {
            Some(score) => score.to_string(),
            None => "?".to_string(),
        };

        f.write_fmt(format_args!(
            "{: ^3} {}{: ^3}{}",
            self.risk_level.to_string().bold(),
            "<".dimmed().italic(),
            entry_score.dimmed().italic(),
            ">".dimmed().italic(),
        ))
    }
}

fn read_data_file(filename: &str) -> Grid<Point> {
    let points = std::fs::read_to_string(filename)
        .unwrap()
        .split('\n')
        .filter(|line| !line.is_empty())
        .map(|line| {
            line.chars()
                .map(|v| Point::new(v.to_digit(10).unwrap()))
                .collect()
        })
        .collect();

    Grid::new(points)
}

fn solve(grid: &mut Grid<Point>) -> u32 {
    let last_x = grid.size_x() - 1;
    let last_y = grid.size_y() - 1;

    let mut point = grid.point_mut(&Position::new(last_x, last_y));
    point.entry_score = Some(point.risk_level);

    let mut last_score = 0;
    let mut this_score = u32::MAX;

    while last_score != this_score {
        last_score = this_score;

        for y in (0..grid.size_y()).rev() {
            for x in (0..grid.size_x()).rev() {
                if x == last_x && y == last_y {
                    continue;
                }

                let current_position = Position::new(x, y);
                let lowest_adjacent_entry_score = &[
                    grid.up_from(&current_position),
                    grid.down_from(&current_position),
                    grid.left_of(&current_position),
                    grid.right_of(&current_position),
                ]
                .iter()
                .map(|other_position| match other_position {
                    Some(other_position) => {
                        grid.point(other_position).entry_score.unwrap_or(u32::MAX)
                    }
                    None => u32::MAX,
                })
                .min()
                .unwrap();

                let mut point = grid.point_mut(&current_position);
                point.entry_score = Some(lowest_adjacent_entry_score + point.risk_level);
            }
        }

        let starting_point = grid.point(&Position::new(0, 0));
        this_score = match starting_point.entry_score {
            Some(score) => score - starting_point.risk_level,
            None => 0,
        };
    }

    this_score
}

fn step_01(filename: &str) -> u32 {
    solve(&mut read_data_file(filename))
}

fn step_02(filename: &str) -> u32 {
    const MULTIPLIER: usize = 5;
    let mut grid = read_data_file(filename);

    for y in 0..grid.size_y() * MULTIPLIER {
        if y >= grid.size_y() {
            grid.points.push(vec![]);
        }

        for x in 0..grid.size_x() * MULTIPLIER {
            if x < grid.size_x() && y < grid.size_y() {
                continue;
            }

            let new_x = if x < grid.size_x() {
                x
            } else {
                x - grid.size_x()
            };

            let new_y = if x < grid.size_x() {
                y - grid.size_y()
            } else {
                y
            };

            let reference_point = grid.point(&Position::new(new_x, new_y));
            let new_risk_level = {
                let risk = reference_point.risk_level + 1;

                if risk > 9 {
                    1
                } else {
                    risk
                }
            };

            let new_point = Point::new(new_risk_level);
            grid.points[y].push(new_point);
        }
    }

    grid.recompute_size();

    solve(&mut grid)
}

fn main() {
    bench(|| {
        let answer = step_01("./data/2021_15_test_tiny.txt");
        print!("step_01_tiny_answer = {}", answer);
        assert_eq!(answer, 8, "step_01 tiny answer");
    });

    bench(|| {
        let answer = step_01("./data/2021_15_test.txt");
        print!("step_01_test_answer = {}", answer);
        assert_eq!(answer, 40, "step_01 test answer");
    });

    bench(|| {
        let answer = step_01("./data/2021_15.txt");
        print!("step_01_real_answer = {}", answer);
        assert_eq!(answer, 415, "step_01 real answer");
    });

    bench(|| {
        let answer = step_02("./data/2021_15_test.txt");
        print!("step_02_test_answer = {}", answer);
        assert_eq!(answer, 315, "step_02 test answer");
    });

    bench(|| {
        let answer = step_02("./data/2021_15.txt");
        print!("step_02_real_answer = {}", answer);
        assert_eq!(answer, 2864, "step_02 test answer");
    });

    bench(|| {
        let answer = step_01("./data/2021_15_kem.txt");
        print!("step_01_kem_answer = {}", answer);
        assert_eq!(answer, 592, "step_01 kem answer");
    });

    bench(|| {
        let answer = step_02("./data/2021_15_kem.txt");
        print!("step_02_kem_answer = {}", answer);
        assert_eq!(answer, 2897, "step_02 kem answer");
    });
}
