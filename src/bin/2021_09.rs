use std::cmp::Ordering;

use aoc::bench;

#[derive(Debug, PartialEq)]
enum VerticalDirection {
    Unknown,
    Higher,
    Equal,
    Lower,
}

#[derive(Debug)]
struct Point {
    value: u64,
    up: VerticalDirection,
    down: VerticalDirection,
    left: VerticalDirection,
    right: VerticalDirection,
    basin_number: Option<usize>,
}

impl Point {
    pub fn new(value: u64) -> Point {
        Point {
            value,
            up: VerticalDirection::Unknown,
            down: VerticalDirection::Unknown,
            left: VerticalDirection::Unknown,
            right: VerticalDirection::Unknown,
            basin_number: None,
        }
    }

    pub fn is_basin(&self) -> bool {
        self.value < 9
    }

    pub fn compare(&self, other: &Point) -> VerticalDirection {
        match self.value.cmp(&other.value) {
            Ordering::Less => VerticalDirection::Lower,
            Ordering::Equal => VerticalDirection::Equal,
            Ordering::Greater => VerticalDirection::Higher,
        }
    }
}

#[derive(Clone, Debug, Default)]
struct Position {
    x: usize,
    y: usize,
}

impl Position {
    pub fn new(x: usize, y: usize) -> Self {
        Self { x, y }
    }
}

#[derive(Debug)]
struct Grid {
    points: Vec<Vec<Point>>,
    size_x: usize,
    size_y: usize,
}

impl Grid {
    pub fn point_for(&mut self, position: &Position) -> &mut Point {
        &mut self.points[position.y][position.x]
    }

    pub fn compare(&self, position_a: &Position, position_b: &Position) -> VerticalDirection {
        self.points[position_a.y][position_a.x].compare(&self.points[position_b.y][position_b.x])
    }

    pub fn left_of(&self, position: &Position) -> Option<Position> {
        if position.x > 0 {
            Some(Position {
                x: position.x - 1,
                y: position.y,
            })
        } else {
            None
        }
    }

    pub fn right_of(&self, position: &Position) -> Option<Position> {
        if position.x + 1 < self.size_x {
            Some(Position {
                x: position.x + 1,
                y: position.y,
            })
        } else {
            None
        }
    }

    pub fn up_from(&self, position: &Position) -> Option<Position> {
        if position.y > 0 {
            Some(Position {
                x: position.x,
                y: position.y - 1,
            })
        } else {
            None
        }
    }

    pub fn down_from(&self, position: &Position) -> Option<Position> {
        if position.y + 1 < self.size_y {
            Some(Position {
                x: position.x,
                y: position.y + 1,
            })
        } else {
            None
        }
    }
}

fn read_data_file(filename: &str) -> Grid {
    let points = std::fs::read_to_string(filename)
        .unwrap()
        .split('\n')
        .map(|v| {
            v.chars()
                .into_iter()
                .map(|v| Point::new(v as u64 - 48))
                .collect::<Vec<Point>>()
        })
        .collect::<Vec<Vec<Point>>>();

    Grid {
        size_x: points[0].len(),
        size_y: points.len(),
        points,
    }
}

fn step_01(filename: &str) -> usize {
    let mut data = read_data_file(filename);

    for x in 0..data.size_x {
        for y in 0..data.size_y {
            let pos = Position::new(x, y);

            if let Some(pos_o) = &data.up_from(&pos) {
                data.points[pos.y][pos.x].up = data.compare(pos_o, &pos);
            }

            if let Some(pos_o) = &data.down_from(&pos) {
                data.points[pos.y][pos.x].down = data.compare(pos_o, &pos);
            }

            if let Some(pos_o) = &data.left_of(&pos) {
                data.points[pos.y][pos.x].left = data.compare(pos_o, &pos);
            }

            if let Some(pos_o) = &data.right_of(&pos) {
                data.points[pos.y][pos.x].right = data.compare(pos_o, &pos);
            }
        }
    }

    let mut lowest_points = 0;

    for x in 0..data.size_x {
        for y in 0..data.size_y {
            let p = &data.points[y][x];

            if (p.up == VerticalDirection::Higher || p.up == VerticalDirection::Unknown)
                && (p.down == VerticalDirection::Higher || p.down == VerticalDirection::Unknown)
                && (p.left == VerticalDirection::Higher || p.left == VerticalDirection::Unknown)
                && (p.right == VerticalDirection::Higher || p.right == VerticalDirection::Unknown)
            {
                lowest_points += (p.value as usize) + 1;
            }
        }
    }

    lowest_points
}

fn assign_basin(data: &mut Grid, basin_number: usize, x: usize, y: usize) {
    let p = Position::new(x, y);
    let mut point = data.point_for(&p);

    if !point.is_basin() {
        return;
    }

    point.basin_number = Some(basin_number);

    for p in vec![
        data.up_from(&p),
        data.down_from(&p),
        data.left_of(&p),
        data.right_of(&p),
    ]
    .into_iter()
    .flatten()
    {
        let pt = &data.points[p.y][p.x];
        if pt.is_basin() && pt.basin_number.is_none() {
            assign_basin(data, basin_number, p.x, p.y);
        }
    }
}

fn step_02(filename: &str) -> usize {
    let mut data = read_data_file(filename);
    let mut basin_number = 0;

    for x in 0..data.size_x {
        for y in 0..data.size_y {
            let p = Position::new(x, y);
            let point = data.point_for(&p);

            if point.is_basin() && point.basin_number.is_none() {
                basin_number += 1;

                assign_basin(&mut data, basin_number, x, y);
            }
        }
    }

    let flattened_points = data
        .points
        .iter()
        .flatten()
        .filter(|p| p.is_basin())
        .collect::<Vec<&Point>>();

    let mut basin_size_counts = (1..=basin_number)
        .map(|basin_index| {
            flattened_points
                .iter()
                .filter(|point| point.basin_number.unwrap() == basin_index)
                .count()
        })
        .collect::<Vec<usize>>();
    basin_size_counts.sort_unstable();
    basin_size_counts.reverse();

    basin_size_counts[0] * basin_size_counts[1] * basin_size_counts[2]
}

fn main() {
    bench(|| {
        let step_01_test_answer = step_01("./data/2021_09_test.txt");
        print!("step_01_test_answer = {}", step_01_test_answer);
        assert_eq!(step_01_test_answer, 15, "step_01 test answer");
    });

    bench(|| {
        let step_01_real_answer = step_01("./data/2021_09.txt");
        print!("step_01_real_answer = {}", step_01_real_answer);
        assert_eq!(step_01_real_answer, 500, "step_01_real_answer");
    });

    bench(|| {
        let step_02_test_answer = step_02("./data/2021_09_test.txt");
        print!("step_02_test_answer = {}", step_02_test_answer);
        assert_eq!(step_02_test_answer, 1134, "step_02 test answer");
    });

    bench(|| {
        let step_02_real_answer = step_02("./data/2021_09.txt");
        print!("step_02_real_answer = {}", step_02_real_answer,);
        assert_eq!(step_02_real_answer, 970200, "step_02 real answer");
    });
}
