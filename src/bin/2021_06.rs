use aoc::bench;

fn read_data_file() -> Vec<i64> {
    std::fs::read_to_string("./data/2021_06.txt")
        .unwrap()
        .split(',')
        .map(|v| v.parse::<i64>().unwrap())
        .collect()
}

fn calculate_fish(data: Vec<i64>, days_to_breed: i64) -> usize {
    let mut buckets = vec![0; 9];

    for value in data {
        buckets[value as usize] += 1;
    }

    for _ in 1..=days_to_breed {
        let bucket_0 = buckets[0];

        for rotation_index in 1..buckets.len() {
            buckets[rotation_index - 1] = buckets[rotation_index];
        }

        buckets[6] += bucket_0;
        buckets[8] = bucket_0;
    }

    buckets.iter().sum()
}

fn step_01() -> usize {
    let data = read_data_file();

    calculate_fish(data, 80)
}

fn step_02() -> usize {
    let data = read_data_file();

    calculate_fish(data, 256)
}

fn main() {
    bench(|| {
        let step_01_answer = step_01();
        print!("step_01 = {:?}", step_01_answer);
        //assert_eq!(5934, step_01_answer); // Test
        assert_eq!(349549, step_01_answer); // Actual
    });

    bench(|| {
        let step_02_answer = step_02();
        print!("step_02 = {:?}", step_02_answer);
        // assert_eq!(26984457539, step_02_answer); // Test
        assert_eq!(1589590444365, step_02_answer); // Actual
    });
}
