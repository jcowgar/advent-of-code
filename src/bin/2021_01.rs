use aoc::bench;

fn read_data_file() -> Vec<u64> {
    std::fs::read_to_string("./data/2021_01.txt")
        .unwrap()
        .split('\n')
        .map(|v| v.parse::<u64>().unwrap())
        .collect()
}

fn count_increases(vec: &[u64]) -> usize {
    (1..vec.len()).filter(|i| vec[*i] > vec[*i - 1]).count()
}

fn step_01() -> usize {
    count_increases(&read_data_file())
}

fn step_02() -> usize {
    let depths = read_data_file();
    let windows: Vec<u64> = (2..depths.len())
        .map(|i| depths[i - 2] + depths[i - 1] + depths[i])
        .collect();

    count_increases(&windows)
}

fn main() {
    bench(|| {
        print!("step_01 = {:?}", step_01());
    });

    bench(|| {
        print!("step_02 = {:?}", step_02());
    });
}
