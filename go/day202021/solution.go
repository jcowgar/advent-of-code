//nolint:forbidigo // this is a cheap console app, fmt print is desired
package day202021

import (
	"fmt"
	"os"
	"sort"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
)

type food struct {
	allergen    mapset.Set[string]
	ingredients mapset.Set[string]
}

type allergen struct {
	name        string
	ingredients mapset.Set[string]
}

func (a *allergen) IsDeterminant() bool {
	return a.ingredients.Cardinality() == 1
}

func (a *allergen) Ingredient() string {
	return a.ingredients.ToSlice()[0]
}

// Input contains multiple lines formatted like:
//
//	mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
func readDataFile(filename string) ([]string, []food, []allergen, error) {
	foods := []food{}
	allergens := make(map[string]allergen)
	allIngredients := mapset.NewSet[string]()

	rawBytes, err := os.ReadFile(filename)
	if err != nil {
		return []string{}, []food{}, []allergen{}, fmt.Errorf("failed to read %s: %w", filename, err)
	}

	raw := string(rawBytes)

	for _, line := range strings.Split(raw, "\n") {
		parts := strings.Split(strings.TrimSuffix(line, ")"), "(contains ")

		foodIngredients := strings.Split(strings.TrimSpace(parts[0]), " ")
		foodAllergens := strings.Split(parts[1], ", ")

		f := food{
			ingredients: mapset.NewSet(foodIngredients...),
			allergen:    mapset.NewSet(foodAllergens...),
		}
		foods = append(foods, f)

		for _, ingredient := range foodIngredients {
			allIngredients.Add(ingredient)
		}

		for _, allergenName := range foodAllergens {
			a, ok := allergens[allergenName]
			if ok {
				a.ingredients = a.ingredients.Intersect(f.ingredients)
			} else {
				a = allergen{
					name:        allergenName,
					ingredients: f.ingredients,
				}
			}

			allergens[allergenName] = a
		}
	}

	allergenSlice := []allergen{}
	for _, v := range allergens {
		allergenSlice = append(allergenSlice, v)
	}

	return allIngredients.ToSlice(), foods, allergenSlice, err
}

func printAllergens(allergens []allergen) {
	for _, a := range allergens {
		fmt.Printf("  allergen: %s\n", a.name)
		fmt.Printf("       all: %s\n", strings.Join(a.ingredients.ToSlice(), ", "))
	}
}

func computeStep1(filename string) int64 {
	allIngredients, foods, allergens, err := readDataFile(filename)
	if err != nil {
		return -1
	}

	safeIngredients := mapset.NewSet(allIngredients...)
	for _, a := range allergens {
		safeIngredients = safeIngredients.Difference(a.ingredients)
	}

	safeOccuranceCount := int64(0)

	for _, f := range foods {
		for _, i := range f.ingredients.ToSlice() {
			if safeIngredients.Contains(i) {
				safeOccuranceCount += 1
			}
		}
	}

	return safeOccuranceCount
}

func remove[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]

	return s[:len(s)-1]
}

func computeStep2(filename string) string {
	_, _, allergens, err := readDataFile(filename)
	if err != nil {
		return ""
	}

	done := []allergen{}

	for len(allergens) > 0 {
		markDone := []int{}
		removeIngredient := []string{}

		for i, a := range allergens {
			if a.IsDeterminant() {
				removeIngredient = append(removeIngredient, a.Ingredient())
				markDone = append(markDone, i)
			}
		}

		for i := len(markDone) - 1; i >= 0; i-- {
			idx := markDone[i]
			done = append(done, allergens[idx])
			allergens = remove(allergens, idx)
		}

		for i := range allergens {
			for _, ingredientName := range removeIngredient {
				if allergens[i].ingredients.Cardinality() > 1 {
					allergens[i].ingredients.Remove(ingredientName)
				}
			}
		}
	}

	sort.SliceStable(done, func(i, j int) bool {
		return done[i].name < done[j].name
	})

	sortedAllergens := []string{}

	for _, a := range done {
		sortedAllergens = append(sortedAllergens, a.Ingredient())
	}

	return strings.Join(sortedAllergens, ",")
}

func Step1Test() int64 {
	return computeStep1("../data/2020_21_test.txt")
}

func Step1() int64 {
	return computeStep1("../data/2020_21.txt")
}

func Step2Test() string {
	return computeStep2("../data/2020_21_test.txt")
}

func Step2() string {
	return computeStep2("../data/2020_21.txt")
}
