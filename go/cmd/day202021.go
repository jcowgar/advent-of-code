package cmd

import (
	"github.com/jcowgar/advent-of-code/go/day202021"
	"github.com/jcowgar/advent-of-code/go/pkg/utils"
	"github.com/spf13/cobra"
)

var day202021Cmd = &cobra.Command{
	Use:   "day202021",
	Short: "Allergen Assessment",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Check("test_01", 5, day202021.Step1Test)
		utils.Check("step_01", 2075, day202021.Step1)
		utils.Check("test_02", "mxmxvkd,sqjhc,fvjkl", day202021.Step2Test)
		utils.Check("step_02", "zfcqk,mdtvbb,ggdbl,frpvd,mgczn,zsfzq,kdqls,kktsjbh", day202021.Step2)
	},
}

func init() {
	rootCmd.AddCommand(day202021Cmd)
}
