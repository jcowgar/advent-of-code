package cmd

import (
	"github.com/jcowgar/advent-of-code/go/day202111"
	"github.com/jcowgar/advent-of-code/go/pkg/utils"
	"github.com/spf13/cobra"
)

var day202111Cmd = &cobra.Command{
	Use:   "day202111",
	Short: "Dumbo Octopus",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Check("test_01", 1656, day202111.Step1Test)
		utils.Check("step_01", 1688, day202111.Step1)
		utils.Check("step_02", 403, day202111.Step2)
	},
}

func init() {
	rootCmd.AddCommand(day202111Cmd)
}
