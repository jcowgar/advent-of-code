package cmd

import (
	"github.com/jcowgar/advent-of-code/go/day202104"
	"github.com/jcowgar/advent-of-code/go/pkg/utils"
	"github.com/spf13/cobra"
)

// day04Cmd represents the day04 command.
var day202104Cmd = &cobra.Command{
	Use:   "day202104",
	Short: "Tic Tac Toe",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Check("step_01", 65325, day202104.Step1)
		utils.Check("step_02", 4624, day202104.Step2)
	},
}

func init() {
	rootCmd.AddCommand(day202104Cmd)
}
