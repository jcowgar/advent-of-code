/*
Copyright © 2022 Jeremy Cowgar <jeremy@cowgar.com>
*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "go",
	Short: "Advent of Code 2021 written in Go",
	Long: `Advent of Code 2021 was original done on each day in the Rust
programming language. I am learning Go at this point and would
like to see how different the solutions may be.

This application does not pretend to look at each original
problem but simply convert the Rust based solutions to go.
In doing so, I am trying to do so in a goish manner.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
