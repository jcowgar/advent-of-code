/*
Copyright © 2022 Jeremy Cowgar <jeremy@cowgar.com>
*/
package cmd

import (
	"github.com/jcowgar/advent-of-code/go/day202101"
	"github.com/jcowgar/advent-of-code/go/pkg/utils"
	"github.com/spf13/cobra"
)

var Day01Cmd = &cobra.Command{
	Use:   "day202101",
	Short: "Sonar Sweep",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Check("step_01", 1288, day202101.Step1)
		utils.Check("step_02", 1311, day202101.Step2)
	},
}

func init() {
	rootCmd.AddCommand(Day01Cmd)
}
