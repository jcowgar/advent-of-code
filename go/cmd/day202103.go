/*
Copyright © 2022 Jeremy Cowgar <jeremy@cowgar.com>
*/
package cmd

import (
	"github.com/jcowgar/advent-of-code/go/day202103"
	"github.com/jcowgar/advent-of-code/go/pkg/utils"
	"github.com/spf13/cobra"
)

// day03Cmd represents the day03 command
var day202103Cmd = &cobra.Command{
	Use:   "day202103",
	Short: "Binary Diagnostic",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Check("step_01", 1131506, day202103.Step1)
		utils.Check("step_02", 7863147, day202103.Step2)
	},
}

func init() {
	rootCmd.AddCommand(day202103Cmd)
}
