/*
Copyright © 2022 Jeremy Cowgar <jeremy@cowgar.com>
*/
package cmd

import (
	"github.com/jcowgar/advent-of-code/go/day202102"
	"github.com/jcowgar/advent-of-code/go/pkg/utils"
	"github.com/spf13/cobra"
)

var day202102Cmd = &cobra.Command{
	Use:   "day202102",
	Short: "Dive!",
	Run: func(cmd *cobra.Command, args []string) {
		utils.Check("step_01", 1947824, day202102.Step1)
		utils.Check("step_02", 1813062561, day202102.Step2)
	},
}

func init() {
	rootCmd.AddCommand(day202102Cmd)
}
