package utils

import (
	"fmt"
	"time"
)

func Check[R comparable](name string, expected R, f func() R) {
	start := time.Now()
	result := f()
	elapsed := time.Since(start)

	fmt.Printf("%s: ", name)

	if result == expected {
		fmt.Printf("pass (value %v)", expected)
	} else {
		fmt.Printf("FAIL: (expected %v but was %v)", expected, result)
	}

	if elapsed.Microseconds() < 1000 {
		fmt.Printf(" in %dµs\n", elapsed.Microseconds())
	} else if elapsed.Milliseconds() < 1000 {
		fmt.Printf(" in %0.3fms\n", float64(elapsed.Microseconds())/1000.0)
	} else {
		fmt.Printf(" in %0.3fs\n", float64(elapsed.Milliseconds())/1000.0)
	}
}
