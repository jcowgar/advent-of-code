package grid

// Square is a multi directionally linked item in a Grid
// containing a user defined value.
//
// Links are in 8 directions:
//
//	Up/Left      Up      Up/Right
//	Left        None        Right
//	Down/Left   Down   Down/Right
type Square[T any] struct {
	Value T

	upLeft    *Square[T]
	up        *Square[T]
	upRight   *Square[T]
	left      *Square[T]
	right     *Square[T]
	downLeft  *Square[T]
	down      *Square[T]
	downRight *Square[T]
}

// UpLeft returns the square (if any) up and to the right of this square.
func (s *Square[T]) UpLeft() *Square[T] {
	return s.upLeft
}

// Up returns the square (if any) directly above this square.
func (s *Square[T]) Up() *Square[T] {
	return s.up
}

// UpRight returns the square (if any) up and to the right of this square.
func (s *Square[T]) UpRight() *Square[T] {
	return s.upRight
}

// Left returns the square (if any) directly to the left of this square.
func (s *Square[T]) Left() *Square[T] {
	return s.left
}

// Right returns the square (if any) directly to the right of this square.
func (s *Square[T]) Right() *Square[T] {
	return s.right
}

// DownLeft returns the square (if any) down and to the right of this square.
func (s *Square[T]) DownLeft() *Square[T] {
	return s.downLeft
}

// Down returns the square (if any) directly below this square.
func (s *Square[T]) Down() *Square[T] {
	return s.down
}

// DownRight returns the square (if any) down and to the right of this square.
func (s *Square[T]) DownRight() *Square[T] {
	return s.downRight
}

// Grid represents a octally linked list with UpLeft, Up, UpRight, Left,
// Right, DownLeft, Down, and DownRight links.
type Grid[T any] struct {
	Squares []Square[T]
	width   int
	height  int
}

// NewGrid creates a new grid of a fixed width and height.
func NewGrid[T any](width, height int) Grid[T] {
	g := Grid[T]{
		width:   width,
		height:  height,
		Squares: make([]Square[T], width*height),
	}

	g.link()

	return g
}

func (g *Grid[T]) Width() int {
	return g.width
}

func (g *Grid[T]) Height() int {
	return g.height
}

func (g *Grid[T]) linkUp(myIdx, upRow, col int) {
	upIdx := upRow*g.width + col
	g.Squares[myIdx].up = &g.Squares[upIdx]

	if col > 0 {
		upLeftIdx := upRow*g.width + col - 1
		g.Squares[myIdx].upLeft = &g.Squares[upLeftIdx]
	}

	if col < g.width-1 {
		upRightIdx := upRow*g.width + col + 1
		g.Squares[myIdx].upRight = &g.Squares[upRightIdx]
	}
}

func (g *Grid[T]) linkDown(myIdx, downRow, col int) {
	downIdx := downRow*g.width + col
	g.Squares[myIdx].down = &g.Squares[downIdx]

	if col > 0 {
		downLeftIdx := downRow*g.width + col - 1
		g.Squares[myIdx].downLeft = &g.Squares[downLeftIdx]
	}

	if col < g.width-1 {
		downRightIdx := downRow*g.width + col + 1
		g.Squares[myIdx].downRight = &g.Squares[downRightIdx]
	}
}

func (g *Grid[T]) link() {
	for row := 0; row < g.width; row++ {
		for col := 0; col < g.height; col++ {
			myIdx := row*g.width + col

			if row > 0 {
				g.linkUp(myIdx, row-1, col)
			}

			if col > 0 {
				leftIdx := row*g.width + col - 1
				g.Squares[myIdx].left = &g.Squares[leftIdx]
			}

			if col < g.width-1 {
				rightIdx := row*g.width + col + 1
				g.Squares[myIdx].right = &g.Squares[rightIdx]
			}

			if row < g.height-1 {
				g.linkDown(myIdx, row+1, col)
			}
		}
	}
}

// NewGridWithSquares creates a new grid of a fixed width and height populated with values.
func NewGridWithSquares[T any](width, height int, values []T) Grid[T] {
	g := NewGrid[T](width, height)

	for i := 0; i < len(values); i++ {
		g.Squares[i].Value = values[i]
	}

	return g
}
