package grid_test

import (
	"testing"

	"github.com/jcowgar/advent-of-code/go/pkg/grid"
	"github.com/stretchr/testify/assert"
)

func TestNewGridWithSquares(t *testing.T) {
	assert.Equal(t, 1, 1)

	g := grid.NewGridWithSquares(3, 3, []int{
		1, 2, 3,
		4, 5, 6,
		7, 8, 9,
	})

	// By testing upper left, middle and lower right we can test all adjoining
	// squares to make sure NewGrid mapped things properly.

	t.Run("1,1 upper left", func(t *testing.T) {
		t.Parallel()

		assert.Equal(t, 1, g.Squares[0].Value, "value")

		assert.Nil(t, g.Squares[0].UpLeft(), "up/left value")
		assert.Nil(t, g.Squares[0].Up(), "up value")
		assert.Nil(t, g.Squares[0].UpRight(), "up/right value")

		assert.Nil(t, g.Squares[0].Left(), "left value")
		assert.Equal(t, 2, g.Squares[0].Right().Value, "right value")

		assert.Nil(t, g.Squares[0].DownLeft(), "down value")
		assert.Equal(t, 4, g.Squares[0].Down().Value, "down value")
		assert.Equal(t, 5, g.Squares[0].DownRight().Value, "down value")
	})

	t.Run("5,5 middle", func(t *testing.T) {
		t.Parallel()

		assert.Equal(t, 5, g.Squares[4].Value, "value")

		assert.Equal(t, 1, g.Squares[4].UpLeft().Value, "up/left value")
		assert.Equal(t, 2, g.Squares[4].Up().Value, "up value")
		assert.Equal(t, 3, g.Squares[4].UpRight().Value, "up/right value")

		assert.Equal(t, 4, g.Squares[4].Left().Value, "left value")
		assert.Equal(t, 6, g.Squares[4].Right().Value, "right value")

		assert.Equal(t, 7, g.Squares[4].DownLeft().Value, "down/left value")
		assert.Equal(t, 8, g.Squares[4].Down().Value, "down value")
		assert.Equal(t, 9, g.Squares[4].DownRight().Value, "down/right value")
	})

	t.Run("9,9 lower right", func(t *testing.T) {
		t.Parallel()

		assert.Equal(t, 9, g.Squares[8].Value, "value")

		assert.Equal(t, 5, g.Squares[8].UpLeft().Value, "up/left value")
		assert.Equal(t, 6, g.Squares[8].Up().Value, "up value")
		assert.Nil(t, g.Squares[8].UpRight(), "up/right value")

		assert.Equal(t, 8, g.Squares[8].Left().Value, "left value")
		assert.Nil(t, g.Squares[8].Right(), "right value")

		assert.Nil(t, g.Squares[8].DownLeft(), "down/left value")
		assert.Nil(t, g.Squares[8].Down(), "down value")
		assert.Nil(t, g.Squares[8].DownRight(), "down/right value")
	})
}
