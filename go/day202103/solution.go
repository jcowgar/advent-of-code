package day202103

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	f "github.com/jcowgar/advent-of-code/go/pkg/functional"
)

func readDataFile() [][]int {
	content, err := ioutil.ReadFile("../data/2021_03.txt")
	if err != nil {
		panic(fmt.Sprintf("Can not read data file: %v", err))
	}

	lines := strings.Split(string(content), "\n")
	result := f.Map(lines, func(v string) []int {
		return f.MapString(v, func(r rune) int {
			if r == '0' {
				return 0
			}

			return 1
		})
	})

	return result
}

func mostCommon(data [][]int, bit int) int {
	var sum int

	for _, v := range data {
		sum += v[bit]
	}

	if sum >= len(data)-sum {
		return 1
	}

	return 0
}

func leastCommon(data [][]int, bit int) int {
	if mostCommon(data, bit) == 1 {
		return 0
	}

	return 1
}

func Step1() int64 {
	var (
		gamma   string
		epsilon string
	)

	data := readDataFile()

	for bit := 0; bit < len(data[0]); bit++ {
		mc := mostCommon(data, bit)
		lc := leastCommon(data, bit)

		gamma += strconv.Itoa(mc)
		epsilon += strconv.Itoa(lc)
	}

	gammaInt, err := strconv.ParseInt(gamma, 2, 64)
	if err != nil {
		panic(fmt.Sprintf("Could not convert '%s' into an integer", gamma))
	}

	epsilonInt, err := strconv.ParseInt(epsilon, 2, 64)
	if err != nil {
		panic(fmt.Sprintf("Could not convert '%s' into an integer", epsilon))
	}

	return gammaInt * epsilonInt
}

func Step2() int64 {
	data := readDataFile()

	bitFilter := func(ary [][]int, comparator func([][]int, int) int) []int {
		for bitFilter := 0; len(ary) > 1; bitFilter++ {
			bitMatch := comparator(ary, bitFilter)
			ary = f.Filter(ary, func(v []int) bool {
				return v[bitFilter] == bitMatch
			})
		}

		return ary[0]
	}

	binArrayToInt := func(ary []int) int64 {
		var value string
		for _, v := range ary {
			value += strconv.Itoa(v)
		}

		result, err := strconv.ParseInt(value, 2, 64)
		if err != nil {
			panic(fmt.Sprintf("Could not convert '%s' into an integer", value))
		}

		return result
	}

	oxygenGeneratorNumbers := bitFilter(data, mostCommon)
	co2ScrubberRatingNumbers := bitFilter(data, leastCommon)

	oxygenI := binArrayToInt(oxygenGeneratorNumbers)
	co2ScrubberI := binArrayToInt(co2ScrubberRatingNumbers)

	return oxygenI * co2ScrubberI
}
