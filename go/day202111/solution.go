package day202111

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"github.com/jcowgar/advent-of-code/go/pkg/grid"
)

const OctopusFlashValue = 10

type Octopus struct {
	Value uint8
}

// Inc increments the luminocity of the octopus and returns if it flashed.
func (o *Octopus) Inc(mySquare *grid.Square[Octopus]) int64 {
	flashes := int64(0)

	o.Value++

	if o.Value == OctopusFlashValue {
		flashes++

		squares := []*grid.Square[Octopus]{
			mySquare.UpLeft(), mySquare.Up(), mySquare.UpRight(),
			mySquare.Left(), mySquare.Right(),
			mySquare.DownLeft(), mySquare.Down(), mySquare.DownRight(),
		}

		for _, sq := range squares {
			if sq != nil {
				flashes += sq.Value.Inc(sq)
			}
		}
	}

	return flashes
}

func (o *Octopus) Reset() {
	o.Value = 0
}

func readDataFile(filename string) grid.Grid[Octopus] {
	content, err := os.ReadFile(filename)
	if err != nil {
		panic(fmt.Sprintf("Could not read data file: %s", err.Error()))
	}

	lines := strings.Split(string(content), "\n")
	height := len(lines) - 1
	width := len(strings.Split(lines[0], ""))
	result := make([]Octopus, height*width)

	for row, line := range lines {
		cols := strings.Split(line, "")

		for col, value := range cols {
			int, err := strconv.ParseInt(value, 10, 8)
			if err != nil {
				panic(fmt.Sprintf("Could not convert %s to a uint8", value))
			}

			result[row*width+col] = Octopus{Value: uint8(int)}
		}
	}

	return grid.NewGridWithSquares(width, height, result)
}

func step(g *grid.Grid[Octopus]) int64 {
	flashes := int64(0)

	// Increment the board
	for i := 0; i < len(g.Squares); i++ {
		sq := &g.Squares[i]

		flashes += sq.Value.Inc(sq)
	}

	// Reset any that flashed
	for i := 0; i < len(g.Squares); i++ {
		if g.Squares[i].Value.Value >= OctopusFlashValue {
			g.Squares[i].Value.Reset()
		}
	}

	return flashes
}

func displayGrid(g *grid.Grid[Octopus]) {
	idx := 0

	for row := 0; row < g.Height(); row++ {
		for col := 0; col < g.Width(); col++ {
			fmt.Printf("%2d ", g.Squares[idx].Value.Value)
			idx++
		}
		fmt.Println()
	}
}

func Step1Test() int64 {
	grid := readDataFile("../data/2021_11_test.txt")
	flashes := int64(0)

	for i := 1; i <= 100; i++ {
		flashes += step(&grid)
	}

	// displayGrid(&grid)

	return flashes
}

func Step1() int64 {
	grid := readDataFile("../data/2021_11.txt")
	flashes := int64(0)

	for i := 1; i <= 100; i++ {
		flashes += step(&grid)
	}

	// displayGrid(&grid)

	return flashes
}

func didAllFlash(g *grid.Grid[Octopus]) bool {
	for i := 0; i < len(g.Squares); i++ {
		if g.Squares[i].Value.Value != 0 {
			return false
		}
	}

	return true
}

func Step2() int64 {
	grid := readDataFile("../data/2021_11.txt")

	for i := int64(1); i <= 100_000; i++ {
		step(&grid)
		if didAllFlash(&grid) {
			return i
		}
	}

	return 0
}
