/*
Copyright © 2022 Jeremy Cowgar <jeremy@cowgar.com>
*/
package main

import "github.com/jcowgar/advent-of-code/go/cmd"

func main() {
	cmd.Execute()
}
