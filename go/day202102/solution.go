package day202102

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

type direction int

const (
	forward direction = iota
	up
	down
)

type movement struct {
	dir   direction
	units int
}

func readDataFile() []*movement {
	content, err := ioutil.ReadFile("../data/2021_02.txt")
	if err != nil {
		panic(fmt.Sprintf("Could not load data file: %v", err))
	}

	lines := strings.Split(string(content), "\n")

	movements := make([]*movement, len(lines))

	for i, v := range lines {
		var dir direction

		fields := strings.SplitN(v, " ", 2)

		switch fields[0] {
		case "forward":
			dir = forward
		case "up":
			dir = up
		case "down":
			dir = down
		default:
			panic(fmt.Sprintf("Movement unknown: '%s'", fields[0]))
		}

		units, err := strconv.Atoi(fields[1])
		if err != nil {
			panic(fmt.Sprintf("Invalid move units: '%s'", fields[1]))
		}

		movements[i] = &movement{
			dir:   dir,
			units: units,
		}
	}

	return movements
}

func Step1() int {
	var (
		horizontal int
		depth      int
	)

	commands := readDataFile()

	for _, command := range commands {
		switch command.dir {
		case forward:
			horizontal += command.units
		case up:
			depth -= command.units
		case down:
			depth += command.units
		}
	}

	return horizontal * depth
}

func Step2() int {
	var (
		aim        int
		horizontal int
		depth      int
	)

	commands := readDataFile()

	for _, command := range commands {
		switch command.dir {
		case forward:
			horizontal += command.units
			depth += aim * command.units
		case up:
			aim -= command.units
		case down:
			aim += command.units
		}
	}

	return horizontal * depth
}
