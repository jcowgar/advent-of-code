package day202101

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
)

func readDataFile() []uint32 {
	content, err := ioutil.ReadFile("../data/2021_01.txt")
	if err != nil {
		panic(fmt.Sprintf("Could not read data file: %s", err.Error()))
	}

	lines := strings.Split(string(content), "\n")
	result := make([]uint32, len(lines))

	for index, v := range lines {
		i, err := strconv.ParseInt(v, 10, 32)
		if err != nil {
			panic(fmt.Sprintf("Could not convert %s to a uint32", v))
		}

		result[index] = uint32(i)
	}

	return result
}

func countIncreases(values []uint32) uint32 {
	var result uint32

	for i := 1; i < len(values); i++ {
		if values[i] > values[i-1] {
			result++
		}
	}

	return result
}

func Step1() uint32 {
	return countIncreases(readDataFile())
}

func Step2() uint32 {
	depths := readDataFile()
	windows := make([]uint32, 0)

	for index := 2; index < len(depths); index++ {
		thisDepth := depths[index-2] + depths[index-1] + depths[index]
		windows = append(windows, thisDepth)
	}

	return countIncreases(windows)
}
