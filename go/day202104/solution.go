package day202104

import (
	"os"
	"regexp"
	"strconv"
	"strings"
)

var numRx *regexp.Regexp

type bingoSquare struct {
	number int
	marked bool
}

type bingoBoard struct {
	squares       []bingoSquare
	calledNumbers []int
	hasBingo      bool
}

// Call a number on a bingo board.
//
// If this number resulted in a Bingo, true is returned
// otherwise false.
func (b *bingoBoard) Call(n int) bool {
	if b.hasBingo {
		return false
	}

	b.calledNumbers = append(b.calledNumbers, n)

	for i := range b.squares {
		sq := &b.squares[i]

		if sq.number == n {
			sq.marked = true

			b.checkBingo()

			break
		}
	}

	return b.hasBingo
}

func allTrue(squares ...bingoSquare) bool {
	isTrue := true

	for _, v := range squares {
		isTrue = isTrue && v.marked
		if !isTrue {
			return false
		}
	}

	return true
}

func (b *bingoBoard) checkBingo() {
	// horizontal
	for i := 0; i < 25; i += 5 {
		if allTrue(b.squares[i : i+5]...) {
			b.hasBingo = true

			return
		}
	}

	// vertical
	for i := 0; i < 5; i++ {
		squares := []bingoSquare{
			b.squares[i],
			b.squares[i+5],
			b.squares[i+10],
			b.squares[i+15],
			b.squares[i+20],
		}

		if allTrue(squares...) {
			b.hasBingo = true

			return
		}
	}
}

func (b *bingoBoard) sumUnmarked() int {
	sum := 0

	for _, sq := range b.squares {
		if !sq.marked {
			sum += sq.number
		}
	}

	return sum
}

type dataFile struct {
	numbers []int
	boards  []bingoBoard
}

func numsFromString(v string) []int {
	if numRx == nil {
		var err error

		numRx, err = regexp.Compile(`(\d+)`)
		if err != nil {
			panic(err)
		}
	}

	result := []int{}
	numsAsStr := numRx.FindAllString(v, -1)

	for _, s := range numsAsStr {
		n, err := strconv.Atoi(s)
		if err != nil {
			panic(err)
		}

		result = append(result, n)
	}

	return result
}

func readDataFile() dataFile {
	result := dataFile{}

	rawContent, err := os.ReadFile("../data/2021_04.txt")
	if err != nil {
		panic(err)
	}

	parts := strings.Split(string(rawContent), "\n\n")
	result.numbers = numsFromString(parts[0])

	for _, b := range parts[1:] {
		board := bingoBoard{}

		for _, n := range numsFromString(b) {
			board.squares = append(board.squares, bingoSquare{number: n})
		}

		result.boards = append(result.boards, board)
	}

	return result
}

func Step1() int64 {
	data := readDataFile()

	for _, called := range data.numbers {
		for i := range data.boards {
			board := &data.boards[i]

			if board.Call(called) {
				return int64(board.sumUnmarked() * called)
			}
		}
	}

	return 0
}

func Step2() int64 {
	var lastWinner *bingoBoard

	data := readDataFile()

	for _, called := range data.numbers {
		for i := range data.boards {
			board := &data.boards[i]

			if board.Call(called) {
				lastWinner = board
			}
		}
	}

	return int64(
		lastWinner.sumUnmarked(),
	) * int64(
		lastWinner.calledNumbers[len(lastWinner.calledNumbers)-1],
	)
}
