import { readFileSync } from 'fs';

interface ToString {
	toString(): string
}

class Position {
	x: number
	y: number

	constructor(x: number, y: number) {
		this.x = x
		this.y = y
	}
}

class Grid<T extends ToString> {
	public points: T[][]

	private _sizeX: number
	private _sizeY: number

	constructor(points: T[][]) {
		this.points = points

		// Does not recognize recompute_size() as initializing _size_x and _size_y
		this._sizeX = 0
		this._sizeY = 0

		this.recompute_size()
	}

	public get sizeY(): number {
		return this._sizeY
	}

	public get sizeX(): number {
		return this._sizeX
	}

	public recompute_size() {
		this._sizeY = this.points.length
		this._sizeX = this.sizeY === 0 ? 0 : this.points[0].length
	}

	public leftOf(position: Position): Position | null {
		if (position.x > 0) {
			return new Position(position.x - 1, position.y)
		} else {
			return null
		}
	}

	public rightOf(position: Position): Position | null {
		if (position.x + 1 < this.sizeX) {
			return new Position(position.x + 1, position.y)
		} else {
			return null
		}
	}

	public upFrom(position: Position): Position | null {
		if (position.y > 0) {
			return new Position(position.x, position.y - 1)
		} else {
			return null
		}
	}

	public downFrom(position: Position): Position | null {
		if (position.y + 1 < this.sizeY) {
			return new Position(position.x, position.y + 1)
		} else {
			return null
		}
	}

	public print() {
		for (let y of this.points) {
			for (let x of y) {
				process.stdout.write(`${x.toString()} `)
			}

			process.stdout.write("\n")
		}
	}
}

class Point implements ToString {
	riskLevel: number
	entryScore: number | null

	constructor(riskLevel: number) {
		this.riskLevel = riskLevel
		this.entryScore = null
	}

	toString(): string {
		return `${this.riskLevel} <${this.entryScore}>`
	}
}

function readDataFile(filename: string): Grid<Point> {
	const points = readFileSync(filename, 'utf-8')
		.split('\n')
		.filter((value) => value !== '')
		.map((line) =>
			line
				.split('')
				.map((item) => Number.parseInt(item, 10))
				.map((risk) => new Point(risk))
		)

	return new Grid<Point>(points)
}

function solve(grid: Grid<Point>): number {
	let lastScore = 0
	let thisScore = Number.MAX_SAFE_INTEGER

	while (lastScore != thisScore) {
		lastScore = thisScore

		for (let y = grid.sizeY - 1; y >= 0; y -= 1) {
			for (let x = grid.sizeX - 1; x >= 0; x -= 1) {
				const isEndPoint = x === grid.sizeX - 1 && y === grid.sizeY - 1
				const currentPosition = new Position(x, y)
				const upPosition = grid.upFrom(currentPosition)
				const downPosition = grid.downFrom(currentPosition)
				const leftPosition = grid.leftOf(currentPosition)
				const rightPosition = grid.rightOf(currentPosition)
				const currentPoint = grid.points[currentPosition.y][currentPosition.x]
				const currentRiskLevel = currentPoint.riskLevel

				if (isEndPoint) {
					currentPoint.entryScore = currentRiskLevel
				} else {
					let lowestAdjacentEntryScore = Number.MAX_SAFE_INTEGER

					if (upPosition != null) {
						const otherPoint = grid.points[upPosition.y][upPosition.x]

						if (otherPoint.entryScore != null) {
							if (otherPoint.entryScore < lowestAdjacentEntryScore) {
								lowestAdjacentEntryScore = otherPoint.entryScore
							}
						}
					}

					if (downPosition != null) {
						const otherPoint = grid.points[downPosition.y][downPosition.x]

						if (otherPoint.entryScore != null) {
							if (otherPoint.entryScore < lowestAdjacentEntryScore) {
								lowestAdjacentEntryScore = otherPoint.entryScore
							}
						}
					}

					if (rightPosition != null) {
						const otherPoint = grid.points[rightPosition.y][rightPosition.x]

						if (otherPoint.entryScore != null) {
							if (otherPoint.entryScore < lowestAdjacentEntryScore) {
								lowestAdjacentEntryScore = otherPoint.entryScore
							}
						}
					}

					if (leftPosition != null) {
						const otherPoint = grid.points[leftPosition.y][leftPosition.x]

						if (otherPoint.entryScore != null) {
							if (otherPoint.entryScore < lowestAdjacentEntryScore) {
								lowestAdjacentEntryScore = otherPoint.entryScore
							}
						}
					}

					currentPoint.entryScore = lowestAdjacentEntryScore + currentPoint.riskLevel
				}
			}
		}

		const startingPoint = grid.points[0][0]
		if (startingPoint.entryScore != null) {
			thisScore = startingPoint.entryScore - startingPoint.riskLevel
		} else {
			thisScore = 0
		}
	}

	return thisScore
}

function step01(filename: string): number {
	const grid = readDataFile(filename)

	const score = solve(grid)
	//grid.print()

	return score
}

function step02(filename: string): number {
	const MULTIPLIER = 5
	const grid = readDataFile(filename)

	for (let y = 0; y < grid.sizeY * MULTIPLIER; y++) {
		if (y >= grid.sizeY) {
			grid.points.push([])
		}

		for (let x = 0; x < grid.sizeX * MULTIPLIER; x++) {
			if (x < grid.sizeX && y < grid.sizeY) {
				continue
			}

			const newX = x < grid.sizeX ? x : x - grid.sizeX
			const newY = x < grid.sizeX ? y - grid.sizeY : y

			const referencePoint = grid.points[newY][newX]
			const incRiskLevel = referencePoint.riskLevel + 1
			const newRiskLevel = incRiskLevel > 9 ? 1 : incRiskLevel

			grid.points[y].push(new Point(newRiskLevel))
		}

	}

	grid.recompute_size()

	const score = solve(grid)
	//grid.print()

	return score
}

function main() {
	let elapsed = 0
	let start = new Date().getTime()

	let step1TestAnswer = step01("../data/2021_15_test.txt")
	elapsed = new Date().getTime() - start
	process.stdout.write(`step1_test_answer = ${step1TestAnswer} (40) in ${elapsed}ms\n`)

	start = new Date().getTime()
	let step1RealAnswer = step01("../data/2021_15.txt")
	elapsed = new Date().getTime() - start
	process.stdout.write(`step1_real_answer = ${step1RealAnswer} (415) in ${elapsed}ms\n`)

	start = new Date().getTime()
	let step2TestAnswer = step02("../data/2021_15_test.txt")
	elapsed = new Date().getTime() - start
	process.stdout.write(`step2_test_answer = ${step2TestAnswer} (315) in ${elapsed}ms\n`)

	start = new Date().getTime()
	let step2RealAnswer = step02("../data/2021_15.txt")
	elapsed = new Date().getTime() - start
	process.stdout.write(`step2_real_answer = ${step2RealAnswer} (2864) in ${elapsed}ms\n`)
}

main()